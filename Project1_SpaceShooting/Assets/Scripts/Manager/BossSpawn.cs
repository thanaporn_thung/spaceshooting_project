﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.InputSystem.HID;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager
{


    public class BossSpawn : MonoBehaviour
    {
   
        [SerializeField] private EnemySpaceship boss;
        [SerializeField] private int bossHp;
        [SerializeField] private int bossSpeed;
        [SerializeField] private RectTransform startDialog;
        [SerializeField] private Button restartButton;
  
        
        public static BossSpawn Instance { get; private set; }

        public void Awake()
        {
            restartButton.onClick.AddListener(OnRestartButton);
       
        }
        
        
        public void Start()
        {
            startDialog.gameObject.SetActive(false);
            SpawnBoss();
            
        }
        
        private void OnRestartButton()
        {
            SceneManager.LoadScene("Game");
        }
        
        private void SpawnBoss()
        {
            var spawnBoss = Instantiate(boss);
            spawnBoss.Init(bossHp, bossSpeed);
            spawnBoss.OnExploded += OnEnemySpaceshipExploded;
        }
        
        private void OnEnemySpaceshipExploded()
        {
            GameManager.Instance.DestroyRemainingShips();
            ScoreManager.Instance.ResetScore();
            startDialog.gameObject.SetActive(true);
           
        }



    }
}
