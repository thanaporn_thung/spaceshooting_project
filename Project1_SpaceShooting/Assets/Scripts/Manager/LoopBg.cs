﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopBg : MonoBehaviour
{
  
   private Material material;
   private Vector2 offset;

   public float xVelocity, yVelocity;

   private void Start()
   {
      offset = new Vector2(xVelocity, yVelocity);
   }

   private void Awake()
   {
      material = GetComponent<Renderer>().material;
   }

   private void Update()
   {
      material.mainTextureOffset += offset * Time.deltaTime;
   }
}
