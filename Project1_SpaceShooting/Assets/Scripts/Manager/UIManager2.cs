﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class UIManager2 : MonoBehaviour
    {

        [SerializeField] private TextMeshProUGUI finalScoreText;
        [SerializeField] private Button restartButton;
        [SerializeField] private RectTransform endDialog;


        private void Awake()
        {

            Debug.Assert(finalScoreText != null, "finalScoreText cannot null");
            Debug.Assert(restartButton != null, "restartButton cannot be null");
            Debug.Assert(endDialog != null, "endDialog cannot be null");

            restartButton.onClick.AddListener(OnRestartButtonClicked);

        }

        private void Start()
        {
            GameManager.Instance.OnRestarted += RestartUI;
            ScoreManager.Instance.OnScoreUpdated += UpdateScoreUI;
            ShowEndDialog(false);
            UpdateScoreUI();
        }

        private void OnRestartButtonClicked()
        {
            SceneManager.LoadScene("Game");
        }

        private void UpdateScoreUI()
        {
            finalScoreText.text = $"Player Score : {ScoreManager.Instance.GetScore()}";
        }

        private void RestartUI()
        {
            ShowEndDialog(true);
        }
        
        private void ShowEndDialog(bool showDialog)
        {
            //UpdateScoreUI();
            endDialog.gameObject.SetActive(showDialog);
        }

        private void OnDestroy()
        {
            GameManager.Instance.OnRestarted -= RestartUI;
            ScoreManager.Instance.OnScoreUpdated -= UpdateScoreUI;
        }
    }
}
